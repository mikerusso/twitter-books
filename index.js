import fs from 'fs'
import csv from 'csv'
import er from 'emoji-regex/RGI_Emoji.js'
import nlp from 'compromise'
import path from 'path'
import { program } from 'commander';
import axios from 'axios'
program.version('0.0.1');



const person = 'miker2049'
// const YEAR_SPLIT = true
// const pic_freq = 5000
//https://stackoverflow.com/a/13396934
const snRegex = /(^|[^@\w])@(\w{1,15})\b/g
const urlRegex=/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/g
const trailRegex = /[ \t]+$/g
const emojiRegex = er();

// parsed fields
// 0 id
// 1 conversation_id
// 2 created_at
// 3 date
// 4 time
// 5 timezone
// 6 user_id
// 7 username
// 8 name
// 9 place
// 10 tweet
// 11 language
// 12 mentions
// 13 urls
// 14 photos
// 15 replies_count
// 16 retweets_count
// 17 likes_count
// 18 hashtags
// 19 cashtags
// 20 link
// 21 retweet
// 22 quote_url
// 23 video
// 24 thumbnail
// 25 near
// 26 geo
// 27 source
// 28 user_rt_id
// 29 user_rt
// 30 retweet_id
// 31 reply_to
// 32 retweet_date
// 33 translate
// 34 trans_src
// 35 trans_dest

//feature requests
// TODO cli interface
// TODO optionally split by year
// TODO Natural language stuff
// TODO block quotes
// TODO instagram support
// TODO images
// issues
// TODO header with book/year


function createTex(user,yearsplit,picfreq) {

  const data = fs.readFileSync(`tweetdata/${user}.csv`,{encoding: 'utf8'})
  // console.log(data)
  const parsed = csv.parse(data,{
    // columns: true,
    relax_column_count: true,
    quote: '',
    delimiter:'\t',
    record_delimiter:'\n'
  }, (err, output)=>{
    processTweets(output,user, yearsplit, picfreq)
  })

}
// const frontmatter = `\\documentclass[12pt]{memoir}
// \\usepackage[utf8x]{inputenc}
// \\usepackage{aeguill}
// \\usepackage{textcomp}
// \\usepackage{csquotes}
// \\begin{document}
// \\title{twitter book: ${person}}`;
function createMetadata(title, person){return `

\\title{${title}}
\\author{${person}}
\\newcommand{\\ISBN}{0-000-00000-2}
\\newcommand{\\press}{beep boop Publishing}
`}

function createFrontmatter(title,person){
  let frontmatterdata = fs.readFileSync('template.tex',{encoding:"utf8"}).toString().split("\n")
  frontmatterdata.splice(33,0,createMetadata(title,person))
  return frontmatterdata.join("\n")
}
// let frontmatter = createFrontmatter("Tweet Book","@shanley")

const ender = `
\\end{document}`

async function processTweets(data,user, yearsplit, picfreq){
  const YEAR_SPLIT = yearsplit;
  const pic_freq = picfreq
  debugger;
  const num = data.length
  let i = num-1;
  let c = 0;
  // get first chapter
  let year = getYear(data[i])
  let lastYear = getYear(data[1])
  let month = getMonth(data[i])
  let title = YEAR_SPLIT ? `@${user}'s tweets: ${year}` : `@${user}'s tweets: ${year}-${lastYear}`
  let author = `@${user}`
  //get first stuff in
  let icount = i - pic_freq/2;
  let text = createFrontmatter(title,user)+createChapter(year)+createSection(month+", "+year);
  while(i>0){
    //creating new paragraphs...
    let par_range = Math.floor(Math.random()*4)+6
    if(c > par_range) {text+="\n\n"; c=0; }
    let thisYear = getYear(data[i])
    if(thisYear!=year){
      year = thisYear;
      if (YEAR_SPLIT) {
        text+=ender;
        fs.writeFileSync(`render/${user}_${year-1}.tex`,text)
        text = createFrontmatter(`${user}'s tweets: ${year}`, author)
      }
      text+=createChapter(year)
    }

    let thisMonth = getMonth(data[i])
    if(thisMonth!=month){
      month = thisMonth;
      text+=createSection(month+", "+year)
      c = 0
    }
    let images = JSON.parse(data[i][14].replaceAll("\'","\""))
    //every 1000 tweets, get an image
    if(i<icount && images.length>0){
      // console.log("download image!")
      let gotIt=await getImage(images[0])
      if (gotIt) {
        text+=`\n\n
\\begin{figure}\n
\\centering
\\includegraphics[width=\\textwidth]{${path.basename(images[0])}}
\\caption{${processTweetText(data[i])}}
\\end{figure}
\n\n
`
        icount-=pic_freq;

      } else {
        text+=processTweetText(data[i])
      }
    } else {
      text+=processTweetText(data[i])
    }
    ++c;
    --i;
  }
  text+=ender;
  if (YEAR_SPLIT) {
    fs.writeFileSync(`render/${user}_${year}.tex`,text)
  } else {
    fs.writeFileSync(`render/${user}.tex`,text)
  }
}

const getImage = async (img) =>{
  let result = false
  await axios({ url: img, method: 'get', responseType:'stream' })
    .catch((err)=>{
      result = false
    })
    .then(function (response) {
      if (response) {
        if(response.data.statusCode ===200) {
          response.data.pipe(fs.createWriteStream("./images/"+path.basename(img)))
          console.log(img)
          result= true
        }
      } else{
        result=false
      }
    })
  return result
}

function processTweetText(entry){
  let twtext = entry[10]
  twtext = deleteMentions(twtext)
  twtext = deleteUrls(twtext)
  twtext = escapes(twtext)
  // return twtext[twtext.length-1].match(/[\.!\?]/) ? twtext : twtext + ".  "
  if (  (twtext[twtext.length-1]==="!")||(twtext[twtext.length-1]==="?")||(twtext[twtext.length-1]===".") ){
    twtext += "  "
  } else {
    twtext += ".  "
  }
  twtext = randomFormat(twtext)
  return twtext
}

function randomFormat(text){
  const test = Math.random() > 0.9;
  let output ='';
  if(test){
    if (Math.random()>0.5) {
      output= italIt(text)
    } else {
      // output= makeQuote(text)
      output=text
    }
  } else{
    output = text
  }
  return output
}

function deleteUrls(text){
  const final = text.replaceAll(urlRegex,"");
  return final;
}
function deleteMentions(text){
  //TODO deal better with mentions
  const final = text.replaceAll(snRegex,"");
  return final;
}

function escapes(text){
  let final =  text.replaceAll(/[^a-zA-Z0-9 \#\?\!\,]/g,'');
  // let final =  text.replaceAll(/([#\$%\^&_\{\}~\\])/g,'\\$1');
  final = final.replaceAll(/[^\x20-\x7E]/g,"");
  final = final.replaceAll(/\#/g,"\\#");
  final = final.replaceAll(/\s+/g," ");
  final = final.replaceAll(trailRegex,"");
  // final = final.replaceAll(/&/g,"\\&");
  // final = final.replaceAll(/\$/g,"\\$");
  // final = final.replaceAll(/"/g,"");
  // final = final.replaceAll(/_/g,"\\_");
  // final = final.replaceAll(/[\{\}\[\]]/g,"");
  return final;
}


function getYear(entry){
  return entry[3].split("-")[0]
}

function getMonth(entry){
  const parsedDate = entry[3].split("-")
  const date = new Date(parsedDate[0],parsedDate[1]-1,parsedDate[2])
  return date.toLocaleString('default', { month: 'long' });
}

function makeQuote(text){
  return `\n\\begin{quote}\n${text}\n\\end{quote}\n`
}

function boldIt(text){
  return `\\textbf${text}`
}

function italIt(text){
  return `\\textit{${text}}`
}

function createChapter(text){
  return `\n\\part[${text}]{${text}}\n`
}

function createSection(text){
  return `\n\\chapter{${text}}\n`
}



program.option('-u, --user <username>', 'the twitter user to generate book for')
       .option('-p, --pic-freq <number>', 'number dictates how often, in tweets, to add picture')
       .option('-s, --split', 'if flagged, will make separate tex files for each year')
program.parse(process.argv)

if(!program.user) console.log('need to add a user to the command! "-u shanley"')
else{
  //we have a user
  const split = program.split ? true : false
  const freq = program.picFreq ? program.picFreq : 250;
  createTex(program.user,  split, freq)
}
