# twitter book
## latex + tweet history

As of now, implicitly uses csvs generated from [twint](https://github.com/twintproject/twint) 
to generate latex files.  

using twint, generate a csv inside the `tweetdata` dir:

```sh
# example twint command
twint -u shanley -o shanley.csv --csv --year 2021
```

## Install and run
```sh
yarn install
# -u, --user required, the twitter user (or rather, the name of the CSV without extension)
# -s, when flagged, split up tex files by year
# -p, this number defines for how many tweets you download and apply an image, so below, about every 2000 tweets a picture will be downloaded
node index.js -u realDonaldTrump -s -p 2000 
```

tex files are then stored in the `render` dir to be compiled

Eventually, will use [compromise](https://github.com/spencermountain/compromise) to be smarter about things.  
